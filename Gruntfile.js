module.exports = function (grunt) {
    grunt.initConfig({
        concat: {
            dist: {
                src: [
                    //'app/templates/assets/js/*.js', // Все JS в папке libs
                    'app/templates/assets/js/jquery.min.js',
                    'app/templates/assets/js/uikit.min.js',
                    'app/templates/assets/js/uikit-icons.min.js',
                    'app/templates/assets/js/jquery.cookie.js',
                    'app/templates/assets/js/fwlone.js',
                ],
                dest: 'app/templates/assets/script.js',
            }
        },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'app/templates/assets/style.css': [
                        'app/templates/assets/css/uikit.css',
                        'app/templates/assets/css/fwlone.css',
                    ]
                }
            }
        },
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['concat', 'cssmin']);

};