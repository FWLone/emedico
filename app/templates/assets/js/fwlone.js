
function removeDesableClass(id){
    console.log(id);
    if(id == 'login'){
        $('#login-button').removeClass('disabled');
    }


    if(id == 'register'){
        $('#register-button').removeClass('disabled');
    }
}

$(document).ready(function () {

    $('input[name="number"]').on('input', function () {
        removeDesableClass($(this).parent().parent().attr('id'));
    });

    $('input[name="name"]').on('input', function () {
        removeDesableClass($(this).parent().parent().attr('id'));
    });

    $('input[name="phone"]').on('input', function () {
        removeDesableClass($(this).parent().parent().attr('id'));
    });

    $('#register-button').on('click', function () {
        $('#register').submit();
    });

    $('#login-button').on('click', function () {
        $('#login').submit();
    });

});