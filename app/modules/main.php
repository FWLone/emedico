<?php
defined('_#FWLone') or die('Restricted access');
if (isset($_GET['fw']) && $_GET['fw'] != '') {

    $pg = $cms->guard($_GET['fw']);
    $pg = $db->safe_sql($pg);

    $page = $db->query("select * from `pages` where `url` = '" . $pg . "' limit 1");
    $page = $db->get_row($page);


    if ($page) {

        $tpl->set('title', $page['title']);
        $tpl->set('keywords', $page['keywords']);
        $tpl->set('description', $page['description']);

        $tpl->set('h1', $page['h1']);
        $tpl->set('content', $page['content']);
        $tpl->set('url', $page['url']);
        $tpl->set('id', $page['id']);

        $template = ($page['template'] == '' ? 'page' : $page['template']);

    } else {

        $tpl->set('title', 'Страница не найдена');
        header("HTTP/1.0 404 Not Found");
        $template = '404';

    }

} else {

    $tpl->set('title', 'Emedico');
    $tpl->set('keywords', 'Emedico');
    $tpl->set('description', '');

    $cats_q = $db->query("select `id`, `title`, `image`,`index_pos` from `cats` where index_show = 1 and index_pos > 0 order by `index_pos` limit 9");
    $cats_arr = array();

    while ($cat = $db->get_row($cats_q)) {
        $cats_arr[$cat['index_pos']] = $cat;
    }

    $tpl->set('cats', $cats_arr);

}