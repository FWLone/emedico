<?php
defined('_#FWLone') or die('Restricted access');
$cms->accessSecure($user);
$tpl->set('title', 'Авторизация');
if (!empty($_POST)) {

    if (isset($_POST['token'])) {

        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $cms->guard($_POST['token']) . '&host=' . $_SERVER['HTTP_HOST']);
        $ulogin = json_decode($s, true);

        $us = $db->query("select * from `users` where `network` = '" . $db->safe_sql($ulogin['network']) . "' and `identy` = '" . $db->safe_sql($ulogin['identity']) . "' limit 1");
        $us = $db->get_row($us);

        $nsess = md5(rand(1111, 1012990) . 'key-fwlone' . TIME);

        if ($us['id']) {

            setcookie('user', $us['salt'], time() + 86400 * 31, '/');
            setcookie('pass', $nsess, time() + 86400 * 31, '/');
            $db->query("UPDATE `users` SET `session` = '" . $nsess . "' WHERE `id`='" . $us['id'] . "'");
            header('location: /');

        } else {

            $salt = GenHash(32, 4);
            $pass = md5(GenHash() . 'Emedico');

            $query = $db->query("INSERT INTO `users` (`login`, `pass`, `salt`, `session`, `first_name`, `last_name`, `network`, `identy`, `regtime`, `lasttime`, `onlinetime`)
                VALUES ('', '" . $pass . "', '" . $salt . "', '" . $nsess . "', '" . $db->safe_sql($ulogin['first_name']) . "', '" . $db->safe_sql($ulogin['last_name']) . "', '" . $db->safe_sql($ulogin['network']) . "', '" . $db->safe_sql($ulogin['identity']) . "', '" . TIME . "', '" . TIME . "', '0')
             ");

            setcookie('user', $salt, time() + 86400 * 31, '/');
            setcookie('pass', $nsess, time() + 86400 * 31, '/');
            header('location: /');

        }

    }

    if (isset($_POST['type'])) {

        if ($_POST['type'] == 'login') {

            $login = $cms->guard($_POST['number']);
            $login = $db->safe_sql($login);

            $pass = $cms->guard($_POST['pass']);
            $pass = md5($db->safe_sql($pass) . 'Emedico');

            if ($login == '') {
                $tpl->set_alert('danger', 'Введите номер!');
            } else if ($pass == '') {
                $tpl->set_alert('danger', 'Введите пароль!');
            } else {
                $us = $db->query("select * from `users` where `login` = '" . $login . "' and `pass` = '" . $pass . "' limit 1");
                $us = $db->get_row($us);
                if ($us['id']) {
                    $nsess = md5(rand(1111, 1012990) . 'key-fwlone' . TIME);
                    setcookie('user', $us['salt'], time() + 86400 * 31, '/');
                    setcookie('pass', $nsess, time() + 86400 * 31, '/');
                    $db->query("UPDATE `users` SET `session` = '" . $nsess . "' WHERE `id`='" . $us['id'] . "'");
                    header('location: /');
                } else {
                    $tpl->set_alert('danger', 'Ошибка при вводите логина или пароля, пользователь с таким сочетанием не найден!');
                }
            }

        }

        if ($_POST['type'] == 'register') {

            $login = $cms->guard($_POST['number']);
            $login = $db->safe_sql($login);

            $pass = $cms->guard($_POST['pass']);
            $pass = md5($db->safe_sql($pass) . 'Emedico');

            $name = $cms->guard($_POST['name']);
            $name = $db->safe_sql($name);

            $us = $db->query("select * from `users` where `login` = '" . $login . "' limit 1");
            $us = $db->get_row($us);

            $nsess = md5(rand(1111, 1012990) . 'key-fwlone' . TIME);

            if ($login == '') {
                $tpl->set_alert('danger', 'Введите номер!');
            } else if ($pass == '') {
                $tpl->set_alert('danger', 'Введите пароль!');
            } else if ($name == '') {
                $tpl->set_alert('danger', 'Введите имя!');
            } else if ($us['id']) {

                $tpl->set_alert('danger', 'Ошибка пользователь с таким номером уже зарегистрирвоан!');

            } else {

                $salt = GenHash(32, 4);

                $query = $db->query("INSERT INTO `users` (`login`, `pass`, `salt`, `session`, `first_name`, `regtime`, `lasttime`, `onlinetime`)
                  VALUES ('" . $login . "', '" . $pass . "', '" . $salt . "', '" . $nsess . "', '" . $name . "', '" . TIME . "', '" . TIME . "', '0')
                ");

                setcookie('user', $salt, time() + 86400 * 31, '/');
                setcookie('pass', $nsess, time() + 86400 * 31, '/');
                header('location: /');

            }

        }

    }
}