<?php
$lng_words = [
    'login-register' => 'Sign in / Sign up',
    'login-button' => 'Sign in',
    'register-button' => 'Sign up',
    'login' => 'Login',
    'register' => 'Registration',
    'number' => 'Telephone number',
    'name' => 'Name',
    'pass' => 'Password',
    'remember-pass' => 'Remember password',
    'lost-pass' => 'Lost password?',
    'other-social' => 'or login via',

    'offer-h1' => 'Select suitable medical <br>equipment on EMEDICO',
    'offer-button' => 'Add place',

    'copyright' => 'Copyright © Egor Hohlov 2018<br> All rights reserved',
    'footer-descrition' => 'All icons and logos of the site, the name of the site, as well as the Emedico logo<br/> are subject to protection by Russian legislation in the field of<br/> intellectual property.',
];