<?php
defined('_#FWLone') or die('Restricted access');
ob_start();

//Тута крч запускаем системку
$cms = new \fw\cms;

//Базу данных
$db = new \fw\db;
$db->start($cms->config('db'));

//Мемкаш
//$cache = new Memcache;
//$cache->connect('127.0.0.1', 11211) or die ("Could not connect to memchache");


if (isset($_GET['lng']) && in_array($_GET['lng'], ['ru', 'en'])) {
    $_SESSION['lng'] = $_GET['lng'];
}

if (!isset($_SESSION['lng'])) {
    $_SESSION['lng'] = 'ru';
}

include H . 'system/lang/' . $_SESSION['lng'] . '.php';



//Шаблонизатор
$tpl = new fw\template(H . $cms->config('site', 'tpl'));
$tpl->set('fwsys', $cms->config('site'));
$tpl->set('lng_words', $lng_words);

//Юзерская
if (isset($_COOKIE['user'], $_COOKIE['pass']) && $_COOKIE['user'] != NULL && $_COOKIE['pass'] != NULL) {

    $salt = $cms->guard($_COOKIE['user']);
    $salt = $db->safe_sql($salt);

    $sess = $cms->guard($_COOKIE['pass']);
    $sess = $db->safe_sql($sess);

    $user_q = $db->query("SELECT * FROM `users` WHERE `salt` = '" . $salt . "' AND `session` = '" . $sess . "' limit 1");
    $user = $db->get_row($user_q);

    if ($user['id']) {

        $ip = $cms->guard($_SERVER['REMOTE_ADDR']);
        $ip = $db->safe_sql($ip);

        $ua = $cms->guard($_SERVER['HTTP_USER_AGENT']);
        $ua = $db->safe_sql($ua);

        $page = $cms->guard($_SERVER['REQUEST_URI']);
        $page = $db->safe_sql($page);

        $db->query("UPDATE `users` SET `lasttime` = '" . TIME . "', " . ($user['lasttime'] > (TIME - 300) ? "`onlinetime`=`onlinetime`+'" . (TIME - $user['lasttime']) . "'," : "") . " `ip` = '" . $ip . "', `useragent` = '" . $ua . "', `page` = '" . $page . "' WHERE `id`='" . $user['id'] . "'");

        //$notifications = $db->query("select `id` from `notification` where `user` = '".$user['id']."' and `view` = '0'");
        //$tpl->set('notifications', $db->get_num_rows($notifications));

        //$messeges = $db->query("select `id` from `messages` where `user` = '".$user['id']."' and `view` = '0'");
        //$tpl->set('messages', $db->get_num_rows($messeges));

    } else {
        setcookie('user', '', time() + 86400 * 31, '/');
        setcookie('pass', '', time() + 86400 * 31, '/');
    }

} else {

    $user = false;

}

$allUsers = $db->query("select `id` from `users`");
$onlineUsers = $db->query("select `id` from `users` where `lasttime` > '" . (TIME - 300) . "'");

$tpl->set('user', $user);
$tpl->set('users_count', $db->get_num_rows($allUsers));
$tpl->set('users_online', $db->get_num_rows($onlineUsers));

//Выход
if (isset($_GET["is_exit"])) {
    if ($_GET["is_exit"] == 1) {
        setcookie('user', '', time() + 86400 * 31, '/');
        setcookie('pass', '', time() + 86400 * 31, '/');
        $db->query("UPDATE `users` SET `session` = '" . md5(rand(1111, 1012990) . 'key-fwlone' . TIME) . "' WHERE `id`='" . $user['id'] . "'");

        header('Location: /');
    }
}

//Хеш для юзеров
function GenHash($length = 12, $strength = 4)
{

    $vowels = 'aeuyio0123456789';
    $consonants = 'bdghjmnpqrstvz';

    if ($strength >= 1) {

        $consonants .= 'BDGHJLMNPQRSTVWXZ';

    } elseif ($strength >= 2) {

        $vowels .= "AEUYIO";

    } elseif ($strength >= 4) {

        $vowels .= '0123456789';

    } elseif ($strength >= 8) {

        $vowels .= '@#$%';
    }

    // Генерируем
    $hash = '';

    $alt = TIME % 2;

    for ($i = 0; $i < $length; $i++) {

        if ($alt == 1) {

            $hash .= $consonants[(rand() % strlen($consonants))];
            $alt = 0;

        } else {

            $hash .= $vowels[(rand() % strlen($vowels))];
            $alt = 1;

        }

    }

    return $hash;

}

//Для множ.
function slv($str, $msg1, $msg2, $msg3)
{

    $str = (int)$str;
    $str1 = abs($str) % 100;
    $str2 = $str % 10;

    if ($str1 > 10 && $str1 < 20) {

        return $str . ' ' . $msg3;

    } elseif ($str2 > 1 && $str2 < 5) {

        return $str . ' ' . $msg2;

    } elseif ($str2 == 1) {

        return $str . ' ' . $msg1;

    }

    return $str . ' ' . $msg3;

}

//Время
function times($times = NULL)
{

    if ((TIME - $times) <= 60) {

        $timesp = slv(((TIME - $times)), 'секунду', 'секунды', 'секунд') . ' назад';
        return $timesp;

    } else if ((TIME - $times) <= 3600) {

        $timesp = slv(((TIME - $times) / 60), 'минуту', 'минуты', 'минут') . ' назад';
        return $timesp;

    } else {

        $today = date("j M Y", TIME);
        $yesterday = date("j M Y", strtotime("-1 day"));
        $timesp = date("j M Y  в H:i", $times);
        $timesp = str_replace($today, 'Сегодня', $timesp);
        $timesp = str_replace($yesterday, 'Вчера', $timesp);
        $timesp = strtr($timesp, array("Jan" => "Янв", "Feb" => "Фев", "Mar" => "Марта", "May" => "Мая", "Apr" => "Апр", "Jun" => "Июня", "Jul" => "Июля", "Aug" => "Авг", "Sep" => "Сент", "Oct" => "Окт", "Nov" => "Ноября", "Dec" => "Дек",));

        return $timesp;

    }

}

function onlineTime($times = 0)
{
    $days = floor($times / (3600 * 24));
    $timesp = '';
    if ($days > 0) {
        $timesp .= slv($days, 'день', 'дня', 'дней') . ', ';
        $times = $times - (3600 * 24 * $days);
    }
    $hours = floor($times / 3600);
    if ($hours > 0) {
        $timesp .= slv($hours, 'час', 'часа', 'часов') . ', ' . slv(floor(($times - (3600 * $hours)) / 60), 'минуту', 'минуты', 'минут');
    } else {
        $timesp .= slv(floor($times / 60), 'минуту', 'минуты', 'минут');
    }

    return $timesp;
}

function rus2translit($string)
{
    $converter = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v',
        'г' => 'g', 'д' => 'd', 'е' => 'e',
        'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'y', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u',
        'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
        'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
        'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

        'А' => 'A', 'Б' => 'B', 'В' => 'V',
        'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
        'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'I', 'Й' => 'Y', 'К' => 'K',
        'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U',
        'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
        'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
        'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

function str2url($str)
{
    $str = rus2translit($str);
    $str = strtolower($str);
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    $str = trim($str, "-");
    $str = trim($str, "/");
    return $str;
}

function tinymce($str)
{
    $str = str_replace("<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n", '', $str);
    $str = str_replace("</body>\r\n</html>", '', $str);
    $str = str_replace("</body>\r\n</html>", '', $str);
    return $str;
}

function str2short($str, $simbols = 200, $before = '...')
{
    $str = strip_tags($str);
    $str = substr($str, 0, $simbols);
    $str = rtrim($str, "!,.-");
    $str = substr($str, 0, strrpos($str, ' ')) . $before;

    return $str;
}

function avatar($id)
{
    if (file_exists(H . 'storage/images/avatar/' . $id . '.png')) {
        return '/storage/images/avatar/' . $id . '.png';
    } else {
        return '/storage/images/no-avatar.svg';
    }
}

function userLevel($level)
{
    $levels = array('Гость', 'Пользователь', 'Редактор', 'Модератор', 'Администратор', 'Создатель');
    if (array_key_exists($level, $levels)) {
        return $levels[$level];
    } else {
        return 'Deleted';
    }
}

function online($time)
{
    if ($time > (TIME - 300)) {
        return 'Online';
    } else {
        return 'Был на сайте ' . times($time);
    }
}


function user($us, $level = false)
{
    return '
	<div class="uk-grid-small uk-flex-middle" uk-grid>
		<div class="uk-width-auto">
			<a href="/users/' . $us['id'] . '"><img class="uk-border-circle" height="40" width="40" src="' . avatar($us['id']) . '"></a>
		</div>
		<div class="uk-width-expand">
			<a href="/users/' . $us['id'] . '"><span class="uk-card-title uk-margin-remove-bottom">' . $us['login'] . '</span></a>
		</div>
	</div>
	';
}

//Cтраница
(isset($_GET['start']) ? $start = intval($_GET['start']) : $start = 0);

//Hавигация
function page($link, $posts, $start, $total, $range = 2)
{
    $pg_cnt = ceil($total / $posts);
    $cur_page = ceil(($start + 1) / $posts);
    $idx_fst = max($cur_page - $range, 1);
    $idx_lst = min($cur_page + $range, $pg_cnt);
    $res = '<ul class="uk-pagination uk-flex-center uk-width-1-1" uk-margin>';
    if ($cur_page != 1) {
        $res .= '<li><a href="' . $link . 'start=' . ($cur_page - 2) * $posts . '"><span uk-pagination-previous></span> Назад</a></li>';
    } else {
        $res .= '<li class="uk-disabled"><a href="#"><span uk-pagination-previous></span> Назад</a></li>';
    }
    if (($start - $posts) >= 0) {
        if ($cur_page > ($range + 1)) {
            $res .= ' <li><a href="' . str_replace('?', '', $link) . '" title="Страница №1">1</a></li>';
            if ($cur_page != ($range + 2)) {
                $res .= '';
            }
        }
    }
    for ($i = $idx_fst; $i <= $idx_lst; $i++) {
        $offset_page = ($i - 1) * $posts;
        if ($i == $cur_page) {
            $res .= ' <li class="uk-disabled"><span>' . $i . '</span></li> ';
        } else {
            $res .= ' <li><a href="' . ($offset_page != 0 ? $link . 'start=' . $offset_page : str_replace('?', '', $link)) . '"  title="Страница №' . $i . '">' . $i . '</a></li>';
        }
    }
    if (($start + $posts) < $total) {
        if ($cur_page < ($pg_cnt - $range)) {
            if ($cur_page != ($pg_cnt - $range - 1)) {
                $res .= '';
            }
            $res .= ' <li><a href="' . $link . 'start=' . ($pg_cnt - 1) * $posts . '" title="Страница №' . $pg_cnt . '">' . $pg_cnt . '</a></li>';
        }
    }
    if ($cur_page != $pg_cnt) {
        $res .= ' <li><a href="' . $link . 'start=' . ($cur_page * $posts) . '">Вперед <span uk-pagination-next></span></a></li>';
    } else {
        $res .= ' <li class="uk-disabled"><a href="#">Вперед <span uk-pagination-next></span></a></li>';
    }
    $res .= '</ul>';
    return $res;
}

