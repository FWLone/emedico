<?php

namespace fw;

class template extends \fw\cms
{

    private $dir_tmpl;
    private $adir_tmpl;
    private $data = array();

    public function __construct($dir_tmpl)
    {
        $this->dir_tmpl = $dir_tmpl;
    }

    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function del($name)
    {
        unset($this->data[$name]);
    }

    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        } elseif (isset($this->data['fwsys'][$name])) {
            return $this->data['fwsys'][$name];
        }
        return "";
    }

    public function display($template)
    {
        if (is_file($this->dir_tmpl . $template . '.html')) {
            require($this->dir_tmpl . $template . '.html');
        } else {
            throw new \Exception('Шаблон не найден: ' . $template);
        }
    }

    public function lng($word)
    {
        if (!empty($this->data['lng_words'][$word])) {
            return $this->data['lng_words'][$word];
        } else {
            return 'Undef';
        }
    }

    public function set_alert($type, $mess)
    {
        $this->data['alert'] = array('type' => $type, 'message' => $mess);
    }

    public function fw_alert()
    {
        if (!empty($this->data['alert'])) {
            return '
			<div class="uk-alert-' . $this->data['alert']['type'] . '" uk-alert>
				<a class="uk-alert-close" uk-close></a>
				<p>' . $this->data['alert']['message'] . '</p>
			</div>';
        } else {
            return '';
        }
    }

    public function userPanel()
    {
        $userPanel = '';

        if (empty($this->data['user'])) {
            $userPanel = '<div class="fw-sign-in uk-inline fw-cat uk-margin-large-right">
                <a href="#sign_in" class="uk-link-reset" uk-toggle>
                    <div class="fw-circle-button"><img src="/storage/images/user.svg"></div>
                    '.$this->lng('login-register').'
                </a>
                <img src="/storage/images/bookmark.svg" class="uk-margin-small-left">
            </div>';
        } else {
            $userPanel = '<div class="fw-sign-in uk-inline fw-cat uk-margin-large-right">
                <a href="/?is_exit=1" class="uk-link-reset"><img src="' . avatar($this->data['user']['id']) . '" height="40" width="40"> ' . $this->data['user']['first_name'] . ' ' . $this->data['user']['first_name'] . ' </a>
                <img src="/storage/images/bookmark.svg" class="uk-margin-small-left">
            </div>';

        }

        return $userPanel;
    }

    public function adminPanel()
    {
        return '
		<div class="uk-section-secondary">
			<div class="uk-container uk-padding-small">
				Админ панель
			</div>
		</div>';
    }

    public function adminSidebar()
    {
        return '
			<ul class="uk-list">
				<li><a href="/admin/"><span uk-icon="icon: home"></span> Панель управления </a></li>
				' . ($this->data['user']['level'] > 2 ? '<li><a href="/admin/users/"><span uk-icon="icon: users"></span> Пользователи </a></li>' : '') . '
				' . ($this->data['user']['level'] == 5 ? '<li><a href="/admin/setting"><span uk-icon="icon: cog"></span> Настройки </a></li>' : '') . '
			</ul>';
    }

    public function breadcrumbs($array = array())
    {
        $bcrumb = '
		<div class="fw-breadcrumb">
		<div class="uk-container">
		<ul class="uk-breadcrumb">
			<li><a href="/">Главная</a></li>';

        if (!empty($array)) {
            foreach ($array as $key => $val) {
                $bcrumb .= '<li><a href="' . $key . '">' . $val . '</a></li>';
            }
        }

        $bcrumb .= '</ul>
		</div></div>';
        return $bcrumb;
    }

}

?>