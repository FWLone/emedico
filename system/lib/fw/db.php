<?php

namespace fw;

class db extends \MySqli {
	public $count_sql = 0;
	public $query = false;
	public $mysqli = false;
	public $error_connect = false;
	public $error_query = false;

	public function start($array){
		$this->mysqli = new \MySqli($array['host'], $array['user'], $array['pass'], $array['base']);
		$this->mysqli->set_charset("utf8");
		if(mysqli_connect_errno()){
			$this->error_connect = true;
			die(mysqli_connect_error());
		}
	}
	
	public function query($query){
		if(!$this->mysqli) $this->connect();
		if(!$this->query = $this->mysqli->query($query)){
			$this->error_query = true;
			die(mysqli_connect_error());
		}
		$this->count_sql++;
		return $this->query;
	}
	
	public function get_row($query){
		if(!$this->mysqli) $this->connect();
		return $query->fetch_assoc();
	}
	
	public function get_num_rows($query){
		if(!$this->mysqli) $this->connect();
		return $query->num_rows;
	}
	
	public function safe_sql($sql){
		if(!$this->mysqli) $this->connect();
		return $this->mysqli->real_escape_string($sql);
	}
	
	public function version(){
		if(!$this->mysqli) $this->connect();
		return $this->mysqli->server_info;
	}
	
	public function close(){
		if($this->mysqli) $this->mysqli->close();
	}
}