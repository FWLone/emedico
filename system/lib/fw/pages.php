<?php
namespace fw;

class pages {

	public function __construct($count,$limit=10){

		$this->js = false;
		$this->count=$count;
		$this->limit=(int)$limit;
		$this->page=$this->page();
		$this->start=$this->start();
		$this->no_post = '';
		$this->k_page=$this->k_page();

	}

	public function page(){

		if (PAGE != NULL){

			if (PAGE == 'last'){

				$page=$this->k_page;

			}else{

				$page=(int)PAGE;
				if ($page == 0)$page = 1;

			}

		}else{

			$page=1;
			
			return $page;
		}
		
	}



	public function start(){
		$start=$this->limit*$this->page;
		$start=(int)($start-$this->limit);
		
		return $start;

	}



	public function k_page(){

		$pages=ceil($this->count/$this->limit);

		return $pages;

	}



	public function out($link){

		$url=null;

		if ($this->count == 0)$url = '';

		if ($this->k_page > 1){

			$url='<ul class="uk-pagination uk-flex-center" uk-margin>';

			if ($this->page > 1)$url=$url.'<li><a href="'.$link.($this->page-1).'"'.($this->js == true?'load="none" page="'.($this->page-1).'"':null).'><span class="uk-margin-small-right" uk-pagination-previous></span> Previous</a></li>';

			else $url=$url.'«Назад';


			$url=$url.'</td><td class="pages" style="width: 30%">';
			$url=$url.$this->page.'/'.$this->k_page;
			$url=$url.'</td><td class="pages" style="width: 35%; border-radius: 0px 6px 6px 0px">';

			if ($this->page < $this->k_page)$url=$url.'<li class="uk-margin-auto-left"><a '.($this->js == true?'load="none" page="'.($this->page+1).'"': null).' href="'.$link.($this->page+1).'">Вперед <span class="uk-margin-small-left" uk-pagination-next></span></a>';

			else $url=$url.'Вперед»';


			$url=$url.'</ul>';

		}

		return $url;

	}

}