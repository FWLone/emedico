<?php
$stime = microtime();
$stime = explode(" ",$stime);
$stime = $stime[1] + $stime[0];

error_reporting(E_ALL);
ini_set('display_errors',true);

define('_#FWLone', true);
define('C', 'UTF-8');
define('TIME', time());
define('H', $_SERVER['DOCUMENT_ROOT'].'/');
define('URL', $_SERVER['REQUEST_URI']);

session_start();
ob_start();


require('system/lib/fw/autoloader.php');
fw\autoloader :: register();

try{

	require(H.'system/common.php');
	
	$router = new fw\router();

	require(H.'app/modules/'.$router -> template.'.php');
	
	$tpl->display('header');
	$tpl->display((isset($template)?$template:$router -> template));
	$tpl->display('footer');
	
} catch (Exception $e) {
	
	die ('Error ' . $e->getMessage());
	
}

$etime = microtime();
$etime = explode(" ", $etime);
$etime = $etime[1] + $etime[0];

echo '<!-- Gen. '.number_format($etime - $stime, 5).' sec-->';
